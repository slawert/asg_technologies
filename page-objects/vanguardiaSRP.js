var mainCommands = {
	clickOnFirstResult: function() {
		return this
		.waitForElementVisible('@link', 1000)
		.click('@link')
	}
};

module.exports = {
	commands: [mainCommands],
	url: function() { 
		return this.api.launchUrl; 
	},
	elements: {
		link: {
			selector: 'div.gsc-webResult .gs-title a'
		}
	}
};