var mainCommands = {
	verifySharingTextEquals: function(query) {
		return this.assert.value('@statusText',query)
	}
};

module.exports = {
	commands: [mainCommands],
	url: function() { 
		return this.api.launchUrl; 
	},
	elements: {
		statusText: {
			selector: '#status'
		}
	}
};