var mainCommands = {
	searchFor: function(query) {
		return this.waitForElementVisible('@magnifier', 1000)
			.click('@magnifier')
			.waitForElementVisible('@searchInput',1000)
			.setValue('@searchInput',query)
			.click('@submit')
	}
};


module.exports = {
	commands: [mainCommands],
	url: function() { 
		return this.api.launchUrl; 
	},
	elements: {
		magnifier: {
			selector: '#header-search-icon'
		},
		searchInput: {
			selector: '.header-search-input'
		},
		submit: {
			selector: '#header-form-search-icon'
		}
	}
};