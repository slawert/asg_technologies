var mainCommands = {
	shareOnTwitter: function(){
		return this
		.waitForElementVisible('@btnTwitter',1000)
		.click('@btnTwitter')
	},
	checkNumberOfCommentsHigherThan: function(browser,number){
		//Scroll hasta el final de la pagina para forzar el lazy loading de los comentarios.
		browser.execute(function () {
    		document.getElementById("footer-logo-url").scrollIntoView();
		}, []);
		return browser.assert.elementPresent('.fyre-comment-article:nth-child('+number+')')
	}
};


module.exports = {
	commands: [mainCommands],
	url: function() { 
		return this.api.launchUrl; 
	},
	elements: {
		btnTwitter: {
			selector: '.btn-twitter'
		}
	}
};