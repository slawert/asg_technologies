#########################
Problema:
#########################

Automatiza el siguiente test case con Selenium usando el patrón de diseño Page Object Model (POM). Para la correcta realización del ejercicio, será necesario definir los Page Objects necesarios y usar los métodos de estos Page Objects desde los tests según convenga.

Ir a lavanguardia.com
Buscar “¿Qué debe tener una buena web?”
Hacer click en el artículo “¿Qué debe tener una buena web?” (el buscador de la vanguardia funciona por pesos, y siempre estará en la primera posición)
Debería tener al menos 3 comentarios (assertion)
Paso opcional: Hacer click en compartir en twitter. Comprobar que abre una nueva ventana y que el link a compartir es el siguiente (assertion): ¿Qué debe tener una buena web?  @lavanguardia http://shr.gs/4y7p5zD


Como enviar el resultado:
Para enviar el resultado del ejercicio, comprime el proyecto en un ZIP y súbelo a Google Drive, Dropbox o el servicio que prefieras y envíanos el link para descargarlo.

Es importante explicar las herramientas utilizadas para la ejecución del test y todo lo necesario para poder lanzar el test y analizar el resultado.


#########################
Solucion:
#########################

1.- Instalar java 8: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

2.- Descargar Selenium Server: https://selenium-release.storage.googleapis.com/3.4/selenium-server-standalone-3.4.0.jar  
3.- Descargar y descomprimir ChromeDriver: https://chromedriver.storage.googleapis.com/index.html?path=2.30/
4.- Colocar el ejecutable en el PATH del OS.

5.- Instalar node.js: https://nodejs.org/en/download/

6.- Instalar NightwatchJS: npm install -g nightwatch (npm viene con empaquetado con node.js, no hay neceisdad de instalarlo nuevamente.)

7.- Abrir la consola/terminal y ejecutar: java -jar selenium-server-standalone-3.4.0.jar 

8.- Abrir la consola/terminal en el directorio en el que se encuentra este fichero y ejecutar: nightwatch

9.- En la consola deberiamos de tener el resultado de nuestro test. Si todo fue bien, deberia de ser algo parecido a:

Lavanguardia Test] Test Suite
==================================

Running:  Twitter sharing la vanguardia
 ✔ Element <#header-search-icon> was visible after 51 milliseconds.
 ✔ Element <.header-search-input> was visible after 590 milliseconds.
  Warn: WaitForElement found 20 elements for selector "div.gsc-webResult .gs-title a". Only the first one will be checked.
 ✔ Element <div.gsc-webResult .gs-title a> was visible after 51 milliseconds.
  Warn: WaitForElement found 3 elements for selector ".btn-twitter". Only the first one will be checked.
 ✔ Element <.btn-twitter> was visible after 132 milliseconds.
 ✔ Testing if element <.fyre-comment-article:nth-child(3)> is present.
 ✔ Testing if value of <#status> equals: "¿Qué debe tener una buena web?  @lavanguardia http://shr.gs/4y7p5zD".

OK. 6 assertions passed. (31.195s)