module.exports = {
  'Twitter sharing la vanguardia' : function (browser) {
	 //Implicit timeout en 5 segundos para generalizar el tiempo de espera en caso de lazy loading.
	 browser.timeoutsImplicitWait(5000)
  	 var vanguardiaM = browser.page.vaguardiaMain();
	 vanguardiaM.navigate().searchFor('¿Qué debe tener una buena web?')
		 
	 var vanguardiaResults = browser.page.vanguardiaSRP();
	 vanguardiaResults.clickOnFirstResult();
	 
	 var vanguardiaP = browser.page.vanguardiaPost();
	 vanguardiaP.shareOnTwitter();
	 vanguardiaP.checkNumberOfCommentsHigherThan(browser,3);
	 
	 //Cambio de ventana.
	 browser.windowHandles(function(result) {
			this.switchWindow(result.value[1]);
	 })
	 var twitterS = browser.page.twitterShare()
	 twitterS.verifySharingTextEquals('¿Qué debe tener una buena web?  @lavanguardia http://shr.gs/4y7p5zD');
	 
	 browser.end();
  }
};


